package team.pi.apple.sc.dashboard.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created on 02/11/2017 17:42
 *
 * @author edwin.zhang
 */
@Controller
public class HystrixController {

    @RequestMapping("/")
    public String home() {
        return "forward:/hystrix";
    }
}
